console.log("Hello");

//In JS, classes can be created using the "class" keyword and {}

/*

Should begin with capital letter

    class <Name>{

    }
*/
// //create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

class Student {
    //
    constructor(name, email, grades){
        this.name = name;
        this.email = email;
        // this.grades = grades;
        this.gradeAve = undefined;
        this.passed = "";
        this.passedWithHonors = "";

        if (
            Array.isArray(grades) &&
            grades.length === 4 &&
            grades.every((grade) => typeof grade === 'number' && grade >= 0 && grade <= 100)
          ) {
            this.grades = grades;
          } else {
            this.grades = undefined;
          }
    }

    //methods
    login(){
        console.log(`${this.email} has log in`);
        return this;
    }

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades(){
        console.log(`${this.name}'s  quarterly grade averages are: ${this.grades}.`)
        return this;
    }

    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum + grade);
        //update property
        this.gradeAve = sum/4;
        return this;
    }

    willPass(){
        this.passed = this.computeAve() >= 85 ? true : false;
        return this
    }

    willPassWithHonors() {
        this.passedWithHonors = this.willPass()
          ? this.computeAve() >= 90
            ? true
            : false
          : undefined;
        return this;
    }
}

let studentOne = new Student('John','john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85] );
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93])


// mini activity
/*
    create a new class called Person

    this person class should be able to instantiate a new object with the ff fields

    name,
    age(should be a number and must be greater than or equal to 18, otherwise set the propertr to undefined),
    nationality,
    address,

    Instantiate 2 new objects from the person class person 1 and person 2

    log both objects in the console.
*/

class Person {
    constructor(name, age, nationality, address) {
      this.name = name;
      this.age = (age >= 18 && typeof age === 'number') ? age : undefined;
      this.nationality = nationality;
      this.address = address;
    }
  }
  

const person1 = new Person("John Doe", 25, "American", "1 Main Street, New York City");


const person2 = new Person("Jane Smith", 17, "Filipino", "1 Commonwealth Avenue, Quezon City");



/* Quiz 1
1. What is the blueprint where objects are created from? Class

2. What is the naming convention applied to classes? PascalCase

3. What keyword do we use to create objects from a class? new

4. What is the technical term for creating an object from a class? Instantiate/Instantiation

5. What class method dictates HOW objects will be created from that
class? constructor method

*/



//Getter and Setter
//est practice dictates that we regulate access to such properties.
//Getter - retrieval
//setter - manipulation

/* Quiz 2

1. Should class methods be included in the class constructor? No
2. Can class methods be separated by commas? No
3. Can we update an object’s properties via dot notation? yes
4. What do you call the methods used to regulate access to an object’s
properties? "access control" or "encapsulation"
5. What does a method need to return in order for it to be chainable? this

*/